# docker-nginx-php

#### 镜像说明
docker镜像构建nginx+php+supervisor

#### 集成PHP插件
php php-opcache php-cli php-devel php-pdo php-pgsql php-sqlite3 php-gd php-curl php-memcached php-imap php-mysqlnd php-mysql php-mbstring php-xml php-zip php-bcmath php-soap php-intl php-readline php-pcov php-msgpack php-igbinary php-ldap php-redis php-pear php-dom php-mcrypt php-odbc php-mssql php-mysqli php-pecl-swoole php-pecl-yaf php-pecl-redis php-fpm Phalcon

#### 目录映射: 
/data/supervisor/tmp ( supervisord.log supervisor.sock supervisord.pid ) # supervisor进程相关目录
/data/supervisor/logs   # supervisor日志目录 
/data/supervisor/conf.d/nginx-php.conf
/data/nginx/conf.d/site.conf
/data/nginx/log #nginx日志目录
/data/php-fpm/logs #php-fpm日志目录
/data/www 站点根目录


#### supervisor配置
```
[program: nginx]
command=/usr/sbin/nginx -g 'daemon off;' 
autorestart=true ; autostart=true ; 
stderr_logfile=/data/supervisor/logs/nginx.error.log ; 
stdout_logfile=/data/supervisor/logs/nginx.stdout.log ; 
user=root ; 
stopsignal=INT startsecs=10 ; 
startretries=5 ; 
stopasgroup=true

[program: php-fpm] 
command=bash -c "sleep 1 && /usr/sbin/php-fpm --fpm-config /etc/php-fpm.conf --pid /data/php-fpm/php-fpm.pid" process_name=%(program_name)s ; 
autorestart=true ; 
autostart=true ; 
stderr_logfile=/data/supervisor/logs/php.error.log ; 
stdout_logfile=/data/supervisor/logs/php.stdout.log ; 
user=root ; 
stopsignal=INT startsecs=10 ; 
startretries=5 ; 
stopasgroup=true
```

#### nginx站点配置
```
server { listen 80; #server_name localhost; root /data/www;

location / {
    #root   public;
    index  index.php index.html index.htm;
    #try_files $uri $uri/ /index.php?$query_string;   #美化URL
}
location ~ \.php$ {
    #root           html;
    fastcgi_pass    unix:/data/php-fpm/php-fpm.sock;
    fastcgi_index  index.php;
    #fastcgi_param  SCRIPT_FILENAME  /data/www$fastcgi_script_name;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    include        fastcgi_params;
}

#error_page 404 /404.html;
#    location = /40x.html {
#}

#error_page 500 502 503 504 /50x.html;
#    location = /50x.html {
#}
}
```


#### 打包命名

docker build -t 用户名/镜像名:版本号 .



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
